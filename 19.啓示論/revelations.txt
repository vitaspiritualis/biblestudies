タイトル: 19　啓示論(B)	
カテゴリ: Bible Study講義案&模擬講義2012～2014年	
説教者: 天宮	
宣布日: 2014-07-19	
本文:

	＜啓示論＞

※「御子本体・分体」の概念を知っている新入生を対象に講義案を作成しています。

【序論(の例)】
啓示とは神様が人間に与える神様の御心と愛のメッセージである。神様も御子も、人間を愛しているから、伝えたいことがたくさんある。そのため、様々な方法でその愛を伝えようとする。神様からの啓示を正しく受け取り、疎通できるようになるためにはどうしたらよいだろうか？

【啓示は救い人の条件によって受けることができる】
ヨハネ1:1-5
　世の初めからあり、神様と共にあった言葉とは、御言葉であり、御子のことを指す。また、「その言は命を与える存在」でもある。私たち人間の霊・魂・肉に命を吹き込んでくださること、それが神様と御子の愛である。

ヨハネ1:14
　その言葉が肉体をもち、実体となった。つまり、御子分体を通して地上に神様を表す肉体が存在し、御子の御言葉が宣布されるようになったことが、神様が人間を、そして一人一人を愛しているという最大の啓示だ。

ヨハネ3:16
　その愛の証として、御子(本体と分体)を地上に送ってくださった。この時代、御子分体を通して神様、御子と出会えることが、最大の啓示であり、愛のメッセージだ。
(※ここまで)

【啓示はその人が準備した器に応じて受ける】
１．	一般的な啓示は誰でも準備するならば、みな受けることができる。

エペソ 1:17
すべての啓示は神様がくださるものであり、御子分体が立てた条件によって啓示の門が開かれるようになったことをはっきりと認める必要がある。神様は各自の個性、使命、専門分野に応じて啓示をくださる。しかし、その啓示は各自が神様を知る水準と霊級と使命によって、次元が異なる。
放送局からは、全家庭に電波を送る。しかし、アンテナを準備して周波数を合わせる人だけが受けることができるように、準備した分に応じて啓示は与えられる。また、壊れたアンテナは修理してこそ正しく電波を受けられるように、正しく啓示を受けるためには、自分の罪を悔い改め、清くなる必要がある。

２．	しかし、時代性にかかわる重要な啓示は、「時代の中心者」だけが受ける。
アモス 3:7、民数記 12:6-8、マタイ11:27
重要なメッセージは通訳官を通して直接伝達するように、とても重要なものは神様と直接通じる人を立てて、その人に神様の御心と計画を伝達してくださる。時代性を復活させるような大きな真理は救い人を通してのみ伝える。一方、各時代の中での次元を上げるための真理は、中心人物、預言者など、その時代に救い人的な働きをする人を通して伝える。時代の中心者は、神様の啓示を正確に受けることで、伝える責任を果たす。私たちは、その啓示を聞いて受け入れる責任を果たさなければならない。


【啓示を与えるための四つの門】
①	＜肉体＞は五感を通して感じることで啓示を受ける。
②	＜心・精神・考え＞は＜考え＞で感じて啓示を受ける。
③	＜魂＞が霊界や魂界で見て感じたことを、＜自分の脳＞に思い浮かぶようにしたり、霊音を聞かせて伝達したりする。
④	＜霊＞が自分の＜魂＞に霊界のことを伝え、＜魂＞がそれを＜自分の肉＞に＜自分の脳＞に思い浮かぶようにしたり、霊音を聞かせて伝達したりする。


【啓示の種類】
１．＜自然啓示＞ 神様が万物を使役者として啓示なさること。
ローマ 1:20神様の見えない性質は被造物によって知ることができる。
イザヤ34:16　万物は世の中で一番大きい本である。

＊聖書での自然啓示の例
箴6：6、民22：28-31、サム下5：23-24、ヨナ4章

・個人にも＜実体の事件＞を通しても悟るようにしてくださり、啓示を伝達される。
・前もって＜兆候＞を見せてくださる。

２．＜超自然啓示＞ 
（１）＜夢＞　魂体が魂的世界で受ける啓示。（一般的な夢と啓示の夢は異なる。）
創世記 37:9、マタイ 2:12、ヨブ 33:15-17など

（２）＜幻＞ 瞬間霊眼が開かれて、ある現象や事件をTV画面のように見る現象。
使 10:3、使 2:17など

（３）＜脳中の幻＞ 脳の中でフィルムが過ぎて行くように、よぎる考え。
ダニエル2:27-28など

（４）＜夢うつつ＞お祈りする時やまどろんでいる時に、おぼろげに見せてくださるもの。
使11:5、使22:17-18

（５）＜異言、異書＞ 霊が話す言葉や霊感を受けて書く文字。
コリントI 12:6、ダニエル 5:24-25

３．＜特別啓示＞ 声、聖書、御言葉で受ける啓示。
（１）＜聖書の啓示＞ 聖書を読むときに多くの悟りと啓示を受ける。聖書は精読、多読、熟読をする必要がある。
テモテII 3:15-17
　聖書を通して、人生について、復活、再臨と引き上げ、聖三位、霊の世界など、さまざまなことを知ることができ、文字を通して啓示を受けることができる。

※先生は(当時)イエス様に「上手く話せるようにしてください」と願ったところ、「聖書を、声を出してはっきりと読みなさい」と言われた。聖書を読むと、言葉の能力と力が生じる。
※少なくとも先生の考えの一部は聖書を土台としている。聖書を読むことでのみ得られる主の心情がある。

＊＜聖書を通して偉大な変化を成した歴史の人物＞
①ルター（ローマ 1:17）
②カルヴィン（ローマ 9:20-21）
➂アウグスティヌス（ローマ 13:12-14）
④リンカーン（マタイ 12:25）

（２）＜音声啓示＞ 声、悟り、感覚、感動で受ける。
創12:1-2、使9:4、使 2:17など

＊＜主と対話する方法＞
旧約では預言者級以上が受けた。今は再臨の霊的時代だから、誰でも主の霊と一対一の対話が可能な時代。まずは自分の考えと心をきれいに空けて精神を集中し、主に心から愛を告白し深い祈りをすれば、誰でも主に会うことができる。諦めず、何度も挑戦し、信じて最後まで求めることが大事だ。「一日に聖三位と主の御名を300回呼ぶ」などと決めて、行なってみることも大きい。先生は対話式のお祈りをされる。最も高い次元の祈り。

（３）＜御言葉の啓示＞ 神様は時代ごとに一人の使命者を立て、核心的な御言葉の啓示を与える。
神様は核心使命者に御言葉の啓示を与える。すべての啓示の中で、御言葉の啓示がもっとも重要。その他の啓示は個人次元の啓示。神様は御言葉で救いを成し、御言葉で時代を転換し、御言葉でサタンを退け、御言葉で霊肉を引き上げさせる。（モーセの十戒、イエス様の御言葉。マタイ11:27）
救い主御子は御言葉を持って表われる。誰が御子の肉体として御言葉を伝え、メシヤとしての働きをするのか？御言葉を聞いてみればわかる。預言者は預言者級の御言葉を伝え、メシヤはメシヤ級の御言葉を伝える。
今は再臨時代だから、最高の啓示は「再臨と引き上げ」に関する啓示。御子は、分体を通して「主日、水曜、聖霊運動、明け方」の壇上を通して、完璧な啓示を伝えている。御言葉を聞く時に、真理の御霊、聖霊様に悟りをくださいと求めることが重要。

【啓示は必ず分別しなければならない】
啓示は三位が与えるものもあるし、自分の霊が悟ったもの、本人が心と対話するもの、協助霊と天使が与えたものもあり、サタンが与えるものもある。すべて主の啓示だと話す。必ず御子分体を通し、確認し、分別すること。

コリントI 2:13、コリントII 11:14

＊＜啓示の分別＞は聖書と御言葉と照らし合わせることが基準。また、現実にそぐわなければならない。そして部分的に合っているとしても、全体を見なければならない。
今は啓示の時だから、多くの人々が短くでも啓示を受け、霊的体験をする。だから、啓示を受けたならば、必ず牧会者や先生に報告し、確認し、よく分別しなければならない。

【結論(の例)】
啓示は神様からの愛のメッセージ。起きている時も寝ている時も神様は私たちに語りかけてくださる。啓示に対する目を開き、その愛を深く悟れるようにしよう。また、神様も御子も、肉体を通して語られる。この時代、御子の分体が生きている時代なので、より直接的に与えられる啓示をはっきりと悟り、行なえるようにしよう。